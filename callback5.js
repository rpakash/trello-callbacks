const fs = require("fs");
const path = require("path");
const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

function callback5(name) {
  const boardFile = path.join(__dirname, "data", "boards.json");

  fs.readFile(boardFile, (err, data) => {
    if (err) {
    } else {
      const personForBoard = JSON.parse(data.toString()).filter((person) => {
        return person.name === name;
      })[0];

      callback1(personForBoard.id, (err, boardData) => {
        if (err) {
          console.log(err);
        } else {
          callback2(personForBoard.id, (err, listData) => {
            if (err) {
              console.log(err);
            } else {
              const lists = listData.filter((list) => {
                return list.name === "Mind" || list.name === "Space";
              });
              let cardsData = [];
              let cards = 0;

              for (let index = 0; index < lists.length; index++) {
                callback3(lists[index].id, (err, cardData) => {
                  cards += 1;

                  if (err) {
                    console.log(err);
                  } else if (cards !== lists.length) {
                    cardsData.push(cardData);
                  } else {
                    cardsData.push(cardData);
                    console.log(boardData);
                    console.log(listData);
                    console.log(cardsData);
                  }
                });
              }
            }
          });
        }
      });
    }
  });
}

module.exports = callback5;
