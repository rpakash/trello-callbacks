const fs = require("fs");
const path = require("path");

function callback1(boardId, callback) {
  const boardFile = path.join(__dirname, "data", "boards.json");

  setTimeout(() => {
    fs.readFile(boardFile, (err, data) => {
      if (err) {
        callback(err, null);
      } else {
        const list = JSON.parse(data).filter((board) => {
          return board.id === boardId;
        })[0];

        callback(null, list);
      }
    });
  }, 2 * 1000);
}

module.exports = callback1;
