const fs = require("fs");
const path = require("path");

function callback2(boardId, callback) {
  const listFile = path.join(__dirname, "data", "lists.json");

  setTimeout(() => {
    fs.readFile(listFile, (err, data) => {
      if (err) {
        callback(err, null);
      } else {
        const list = JSON.parse(data)[boardId];

        callback(null, list);
      }
    });
  }, 2 * 1000);
}

module.exports = callback2;
