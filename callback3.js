const fs = require("fs");
const path = require("path");

function callback3(listId, callback) {
  const cardsFile = path.join(__dirname, "data", "cards.json");

  setTimeout(() => {
    fs.readFile(cardsFile, (err, data) => {
      if (err) {
        callback(err, null);
      } else {
        const list = JSON.parse(data)[listId];

        callback(null, list);
      }
    });
  }, 2 * 1000);
}

module.exports = callback3;
