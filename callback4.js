const fs = require("fs");
const path = require("path");
const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

function callback4(name, listName) {
  const boardFile = path.join(__dirname, "data", "boards.json");

  fs.readFile(boardFile, (err, data) => {
    if (err) {
    } else {
      const personForBoard = JSON.parse(data.toString()).filter((person) => {
        return person.name === name;
      })[0];

      callback1(personForBoard.id, (err, boardData) => {
        if (err) {
          console.log(err);
        } else {
          callback2(personForBoard.id, (err, listData) => {
            if (err) {
              console.log(err);
            } else {
              const mind = listData.filter((list) => {
                return list.name === listName;
              })[0];

              callback3(mind.id, (err, cardsData) => {
                if (err) {
                  console.log(err);
                } else {
                  console.log(boardData);
                  console.log(listData);
                  console.log(cardsData);
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = callback4;
